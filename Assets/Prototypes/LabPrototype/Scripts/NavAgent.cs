﻿using UnityEngine;
using System.Collections;

public class NavAgent : MonoBehaviour
{
	
	public Transform mTarget;

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
		GetComponent<NavMeshAgent>().destination = mTarget.position;
	}
}
