﻿using UnityEngine;
using System.Collections;

public class TriggerCube : MonoBehaviour 
{
	
	public Camera topCam;
	public Camera amoebaCam;
	public GameObject door;
	
	public GameObject Scientist;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	
	void OnTriggerEnter(Collider col)
	{
		if(col.gameObject.tag == "Amoeba")
		{
			amoebaCam.enabled = false;
			Scientist.gameObject.GetComponent<CharacterMotor>().canControl = true;;
		}
		
		if (col.gameObject.tag == "Scientist")
		{
			topCam.enabled = false;
		}
	}
	
	
}
