﻿using UnityEngine;
using System.Collections;

public class AmoebaController : MonoBehaviour
{
	Vector3 mInitialScale;
	CharacterController mAmoebaCharacter;
	
	public float mCrouchScale = 0.25f;
	
	// :::::::::::::::::::::: Actions
	public bool isCrouching;
	
	// :::::::::::::::::::::: Movement
	public float speed = 6.0F;
    public float jumpSpeed = 8.0F;
    public float gravity = 20.0F;
    private Vector3 moveDirection = Vector3.zero;
	
	
	
	// Use this for initialization
	void Start ()
	{
		mInitialScale = this.transform.localScale;
		mAmoebaCharacter = transform.GetComponent<CharacterController>();
	}
	
	// Update is called once per frame
	void Update ()
	{
		amoebaControlLoop ();
	}
	
	
	
	
	// ::::::::::::::::::::::::::::::::::::::: METHODS
	void amoebaControlLoop ()
	{
		Move();
		Crouch ();
	}
	
	
	void Move()
	{
//		Vector3 moveDir = transform.TransformDirection(Vector3.left);
//		float curSpeed = mSpeed * Input.GetAxis("Horizontal");
//		mAmoebaCharacter.SimpleMove(moveDir * curSpeed);
		
		
		if (mAmoebaCharacter.isGrounded) {
            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, 0);
            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection *= speed;
            if (Input.GetButton("Jump"))
                moveDirection.y = jumpSpeed;
            
        }
		
		if (!mAmoebaCharacter.isGrounded)
		{

		}
		
        moveDirection.y -= gravity * Time.deltaTime;
       mAmoebaCharacter.Move(moveDirection * Time.deltaTime);

	}
	
	void Crouch ()
	{
		if (Input.GetKey (KeyCode.DownArrow)) {
			isCrouching = true;
			
			Vector3 tempScale = transform.localScale;
			tempScale.y = mCrouchScale;
			transform.localScale = tempScale;
			mAmoebaCharacter.radius = .15f;
			
		} else {
			
			if (isCrouching == true) {
				Vector3 tempScale = transform.localScale;
				Vector3 tempPos = transform.position;
				
				tempPos.y = transform.position.y + .05f;
				tempScale.y = mInitialScale.y;
				
				transform.position = tempPos;
				transform.localScale = tempScale;
				
				mAmoebaCharacter.radius = .5f;
				isCrouching = false;
			}
		}
	}
}
