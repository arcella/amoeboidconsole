﻿using UnityEngine;
using System.Collections;

public class Item_Script : MonoBehaviour {

	public bool			InteractiveItem=false;
	public bool			MovableItem=false;
	public bool			QuestItem=false;
	
	private string		InteractiveTag="Interactive";
	private string		MovableTag="Movable";
	
	public bool		isHeld = false;
	
	
	// Use this for initialization
	void Awake () {
		if (InteractiveItem) {
			this.gameObject.tag = InteractiveTag;
		}
		
		if (MovableItem) {
			this.gameObject.tag = MovableTag;
		}
	}
	
	
	// Update is called once per frame
	void Update () {
	
	}
	
	
	void PickUp(GameObject Parent) {
		if (isHeld) {
			//isHeld=false;
			this.gameObject.rigidbody.useGravity=false;
			this.gameObject.rigidbody.velocity=Vector3.zero;
			//this.gameObject.transform.position=Parent.transform.position;
			iTween.MoveTo( this.gameObject, iTween.Hash("position", Parent.gameObject.transform.position, "oncomplete", "OnMoveToComplete(Parent)", "time", 0.1f) );
			this.gameObject.transform.parent=Parent.gameObject.transform;
			collider.isTrigger = true;
		}
		else if (!isHeld) {
			isHeld=true;
			this.gameObject.rigidbody.useGravity=true;
			this.gameObject.transform.parent=null;
			collider.isTrigger = false;
		}
	}
	
	void OnMoveToComplete(GameObject ParentAgain) {
		this.gameObject.transform.position = ParentAgain.gameObject.transform.position;
	}
	
	public void Drop()
	{
		this.gameObject.rigidbody.useGravity = true;
		collider.isTrigger = false;
		isHeld = false;
		this.gameObject.transform.parent = null;
		Debug.Log("DROPPED item");
	}
	
}
