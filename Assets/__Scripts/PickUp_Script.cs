using UnityEngine;
using System.Collections;

public class PickUp_Script : MonoBehaviour
{
	
	//attach cursor2.png to this (or w/e cursor you want)
	public Texture				CursorTexture;
	//
	
	private float				CursorSize = 10.0f;
	private float 				rX = (Screen.width) / 2;
	private float 				rY = (Screen.height) / 2;
	
	//attach the gameObject in front of the first-person controller to this; this is where the item will be held
	public GameObject			HeldSpace;
	//
	
	private float				MoveTime = 2.0f;
	private RaycastHit 			hit;
	private float				PickUpDistance = 10f;
	private GameObject			heldObject;
	private bool				isHolding;
	
	void OnGUI ()
	{
		//draw the cursor
		GUI.DrawTexture (new Rect (rX, rY, CursorSize, CursorSize), CursorTexture, ScaleMode.StretchToFill);
	}
	
	
	// Use this for initialization
	void Awake ()
	{
		Screen.showCursor = false;
	}
	
	void Update ()
	{
				
		grabObject();

	}
	
	void grabObject ()
	{
		//raycast!
		Physics.Raycast (Camera.main.ScreenPointToRay (new Vector3 (rX, rY, 0)), out hit, PickUpDistance);

		if (hit.collider != null) 
		{
			
			//Debug.Log ("past the collider");
			if (hit.collider.gameObject.tag == "Movable") 
			{
				
				if (Input.GetKeyDown (KeyCode.E) || (Input.GetButton ("Grab"))) 
				{ 
					hit.collider.gameObject.SendMessage ("PickUp", HeldSpace);
					heldObject= hit.collider.gameObject;
				}
			}
		}
		
		
		// LET GO
		if(Input.GetButtonUp("Grab"))
		{
			heldObject.GetComponent<Item_Script>().Drop();
			//heldObject = null;
		}
		

	}
}
