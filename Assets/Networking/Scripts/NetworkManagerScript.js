#pragma strict

var gameName:String = "Amoeboid Networking Test";
private var refreshing:boolean;
private var hostData:HostData[];

var btnX:float;
var btnY:float;
var btnW:float;
var btnH:float;



function Start () 
{
	btnX = Screen.width * 0.05;
	btnY = Screen.width * 0.05;
	btnW = Screen.width * 0.1;
	btnH = Screen.width * 0.1;

}

function Update () 
{
	if(refreshing)
	{
		if(MasterServer.PollHostList().Length > 0)
		{
			refreshing = false;
			Debug.Log(MasterServer.PollHostList().Length);
			hostData = MasterServer.PollHostList();
		}
	}
}


function startServer()
{
	Network.InitializeServer(32,25001, !Network.HavePublicAddress);
	MasterServer.RegisterHost(gameName,"Amoeboid Test", "AmoeboidGame");
	
}

function refreshHostList()
{
	MasterServer.RequestHostList(gameName);
	refreshing = true;
	
}


// Messages
function OnServerInitialized()
{
	Debug.Log("Server Initialized");
}



function OnMasterServerEvent(mse:MasterServerEvent)
{
	if(mse == MasterServerEvent.RegistrationSucceeded )
	{
		Debug.Log("Registered Server");
	}
}



// GUI
function OnGUI()
{
	if(!Network.isClient && !Network.isServer)
	{
		if(GUI.Button(Rect(btnX,btnY,btnW,btnH),"Start Server"))
		{
			Debug.Log("Starting Server");
			startServer();
		}
		
		
		if(GUI.Button(Rect(btnX,btnY * 1.2 + btnH,btnW,btnH),"Refresh Hosts"))
		{
			Debug.Log("Refreshing");
			refreshHostList();
		}
		
		if(hostData){
			for(var i:int = 0; i < hostData.length; i++)
			{
				if(GUI.Button(Rect(btnX * 1.5 + btnW, btnY * 1.2 + (btnH * i), btnW * 3, btnH * 0.5), hostData[i].gameName))
				{
					Network.Connect(hostData[i]);
				}
			}
		}
	}
}